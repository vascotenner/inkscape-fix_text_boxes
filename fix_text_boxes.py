#! /usr/bin/python
'''
Copyright (C) 2014 Vasco Tenner, tenner@physics.leidenuniv.nl

This inkscape extension moves the flow_box of a flowed text to the
position of the flowed text. It can also move the box outside the page
area.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex, simpletransform
import gettext
_ = gettext.gettext

class Embedder(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("-p", "--put-outside",
            action="store", type="inkbool", 
            dest="put_outside", default=False,
            help="Put text box outside page area")

        self.OptionParser.add_option("-s", "--selectedonly",
            action="store", type="inkbool", 
            dest="selectedonly", default=False,
            help="move only selected boxes")


    def effect(self):
        # if slectedonly is enabled and there is a selection only embed selected
        # images. otherwise embed all images
        if (self.options.selectedonly):
            self.embedSelected(self.document, self.selected)
        else:
            self.embedAll(self.document)

    def embedSelected(self, document, selected):
        self.document=document
        self.selected=selected
        if (self.options.ids):
            for id, node in selected.iteritems():
                if node.tag == inkex.addNS('flowRoot'):
                    self.moveBox(node)

    def embedAll(self, document):
        self.document=document #not that nice... oh well
        path = '//svg:flowRoot'
        for node in self.document.getroot().xpath(path, namespaces=inkex.NSS):
            self.moveBox(node)

    def moveBox(self, node):
        transform = node.get(inkex.addNS('transform'))
        matrix = simpletransform.parseTransform(transform)

        if self.options.put_outside:
            height = 0
            # find the textbox
            for child in node.xpath('//svg:flowRegion', 
                    namespaces=inkex.NSS)[0].getchildren():
                y = float(child.get('y'))
                height = float(child.get('height'))
                #move rect outside page
                child.set('y', '%f'%(-height))

            # add movement to translate
            matrix[1][2] += y+height
            node.set('transform', simpletransform.formatTransform(matrix))
        else:
            # find the textbox
            for child in node.xpath('//svg:flowRegion', 
                    namespaces=inkex.NSS)[0].getchildren():
                x = float(child.get('x'))
                y = float(child.get('y'))
                #add translate to rect
                child.set('x', '%f'%(x+matrix[0][2])) 
                child.set('y', '%f'%(y+matrix[1][2]))

            #remove translate from transform
            matrix[0][2] = 0
            matrix[1][2] = 0
            node.set('transform', simpletransform.formatTransform(matrix))

if __name__ == '__main__':
    e = Embedder()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 encoding=utf-8 textwidth=99
