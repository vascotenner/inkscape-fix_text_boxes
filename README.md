This inkscape extension moves the flow_box of a flowed text to the
position of the flowed text. It can also move the box outside the page
area.
